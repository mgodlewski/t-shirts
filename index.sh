#!/bin/bash

[ $# -ne 1 ] && echo "usage: $0 buildDirectory" && exit 1

buildDirectory=$1

[ ! -d $buildDirectory ] && mkdir $buildDirectory

cp -r assets/* $buildDirectory

cat << EOF > ${buildDirectory}/index.html
<html>
<head>
<title>Les T-Shirts de Mathieu</title>
<style>
.t-shirt {
  background-size: 400px;
  background-repeat: no-repeat;
  text-align: center;
  text-valign: middle;
  width: 400px;
  height: 400px;
  padding-top: 100px;
  float: left;
}
</style>
</head>
<body style="display: flex; flex-wrap: wrap;">
<div style="width: 90vw; display: flex; justify-content: center;"><h1>Les T-Shirts de Mathieu</h1></div>
EOF

# Content
for svg in $(ls -1t *.svg)
do
  flocage=$(basename $svg .svg)
  diplayName=$(echo $flocage | sed 's/\([A-Z0-9]\)/ \1/g')
  echo "Generate pngs for $flocage"
  inkscape -f ${svg} -d 100 -e ${buildDirectory}/${flocage}_100.png > /dev/null
  inkscape -f ${svg} -d 300 -e ${buildDirectory}/${flocage}_300.png > /dev/null
  cp ${svg} ${buildDirectory}/
(
[ -f ${flocage}.properties ] && source ${flocage}.properties
[ -z "$BACKGROUND" ] && BACKGROUND="tshirt-front-white"
cat << EOF >> ${buildDirectory}/index.html
<a name="${flocage}"/>
<div style="width: 45vw; display: flex; justify-content: flex-end;">
  <div class="t-shirt" style="background-image: url(images/${BACKGROUND}.png);">
    <img width="150px" src="${flocage}_100.png"/>
  </div>
</div>
<div  style="width: 45vw">
  <h2>${diplayName}</h2>
  <a href="${flocage}.svg">SVG</a> -
  <a href="${flocage}_300.png">PNG</a>
  <p>$DESCRIPTION</p>
</div>
EOF
)
done

# Footer
cat << EOF >> ${buildDirectory}/index.html
<div style="width: 90vw; display: flex; justify-content: center;">
<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/fr/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/3.0/fr/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/fr/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 3.0 France</a>
</div>
</body>
</html>
EOF
